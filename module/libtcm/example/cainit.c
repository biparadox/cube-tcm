#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "data_type.h"
#include "cube.h"

#include "tcm_constants.h"
#include "app_struct.h"
#include "pik_struct.h"
#include "sm4.h"
#include "tcmfunc.h"
#include "vtcm_alg.h"

char * prikeyfile="CApri.key";
char * pubkeyfile="CApub.key";

int main(int argc,char **argv)
{

    int ret;
   
    int i,j;


    ret=_TSMD_Init();

    ret= TCM_LibInit(); 


    ret=TCM_ExCreateCAKey();
    if(ret!=0)
    {
		printf("TCM_ExCreateCAKey failed!\n");
		return -EINVAL;	
    }	

    ret=TCM_ExSaveCAPriKey(prikeyfile);
    if(ret!=0)
    {
		printf("TCM_ExSaveCAPriKey failed!\n");
		return -EINVAL;	
    }	
		
    ret=TCM_ExSaveCAPubKey(pubkeyfile);
    if(ret!=0)
    {
		printf("TCM_ExSaveCAPubKey failed!\n");
		return -EINVAL;	
    }	

    return ret;	

}

