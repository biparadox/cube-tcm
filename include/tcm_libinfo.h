#ifndef TCM_LIBINFO_H
#define TCM_LIBINFO_H

/*
    tcm info type
*/
enum type_tcm_libinfo
{
	TYPE(TCM_LIBINFO)=0x2210,
};

/*
    tcm info subtype
*/
enum subtype_tcm_libinfo
{
	SUBTYPE(TCM_LIBINFO,LIBINFO)=0x01,
};

/*
    tcm info struct
*/
typedef struct tagtcm_libinfo_libinfo
{
	char dev_name[DIGEST_SIZE];  // if type is 0 or 1, dev_name is /dev/tcm* or /dev/tpm*
                                 // if type is 2, dev_name is ip address:port
    int type;       // 0 : tcm device
                    // 1 : vtcm device 
                    // 2 : tcp conn
	BYTE node_uuid[DIGEST_SIZE];    // if type is 0 or 2, node_uuid is machine uuid
                                    // if type is 1, node_uuid is sm3(machine_uuid+proc_name)
	BYTE node_name[DIGEST_SIZE]; 
}__attribute__((packed)) RECORD(TCM_LIBINFO,LIBINFO);
#endif
